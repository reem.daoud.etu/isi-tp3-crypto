## Binome

FADEL, Salwa, email: salwa.fadel.etu@univ-lille.fr

DAOUD, Reem, email: reem.daoud.etu@univ-lille.fr


### Partie 1 : Responsabilité partagée

### Question 1 :
   
   Pour la réalisation de cette étape on dispose des éléments suivants : 
	 - un disk qui contient le fichier chiffré/déchiffré.
	 - ramdisk vide
	 - le responsable 1 qui a une clé usb1 qui contient sa clé generée "maCle.pem" . 
	 - le responsable 2 qui a une clé usb2 qui contient sa clé generée "maCle.pem" .
	
    Notre solution consiste à chiffrer le fichier qui contient les donneés sensibles avec une cle (keymaster) puis on genere  
    aléatoirement une clé pour chaque responsable et on chiffre la cle (keymaster)
    en deux étapes :
    le premier chiffrement avec la cle du premier responsable1 et le deuxieme chiffrement avec la cle du deuxieme responsable2.
    Et chaque responsable va choisir son propre mot de pass (comme ça on assurera l'authentification à deux facteurs cle + mot de passe)  
    on va concatener les mot de passe et on rechiffre encore le keymaster chiffré précedemment avec cette concaténation de mot de pass. 
    De cette facon pour déchiffrer on a besoin de presenter les deux responsables avec leurs clé usb qui contient leurs cle et 
    leurs mot de passe on déchiffre tout d'abbord avec la premiere cle, ensuite avec l'autre cle et apres avec la concatenation 
    de leurs deux mot de pass.
    	
### Question 2 :
      
      ## fichier crypt.sh (pour chiffre):
  
    	- On chiffre le fichier avec une clé keymaster générée et stockée sur le ramdisk et une fois on chiffre le fichier, on 
      supprime le fichier en clair qui est sur le disk. 
    	- On genere une cle pour responsabe1 et on la met dans usb1.
    	- On genere une cle pour responsabe2 et on la met dans usb2.
    	- On rechiffre le fichier avec la cle du resposable 1 .
    	- On rechiffre le fichier avec la cle de resposable 2 .
    	- On rechiffre le fichier avec la concatenation de leurs deux mot de pass.
    	- On supprime les fichiers du ramdisk.
    	
      ## fichier decrypt.sh (pour déchiffre):
      
        - On demande pour chaque responsable de taper leur mot de pass.
        - On déchiffre avec la cle du premier responsable le fichier chiffré que ça soit celui dans usb1 ou usb2 et on le stock 
        dans ramdisk.
        - On déchiffre avec la deuxieme clé .
        - On déchiffre avec avec la concatenation des deux mot de pass.   
        - Enfin on décrypte le fichier crypt avec la clé keymaster pour obtenir le fichier orginal.
        
        
### Partie 2 : Délégation de droit

### Question 3 :
   
	- On dispose d'un disk qui contient le fichier chiffré/déchiffré.
	- un ramdisk vide
	- le responsable 1 qui a une clé usb1 qui contient sa clé generée "maCle.pem" . 
	- le responsable 2 qui a une clé usb2 qui contient sa clé generée "maCle.pem" .
	- le representant du responsable 1 qui a une clé usb3 qui contient sa clé generée "maCle.pem" .
	- le representant du responsable 2 qui a une clé usb4 qui contient sa clé generée "maCle.pem" .
	
    Notre solution est la suivante :
    
    On chiffre notre fichier avec une cle (keymaster) et on sait que les combinaisons possible pour dechiffrer sont :   
    technical password avec Juridique password
    technical password avec représentant juridique password
    Juridique password avec représentant technique password   
    représentant juridique password avec représentant technical password 
    donc si on veut résumer toutes les combinaisons possibles on aura : 
    cle1 avec cle2 ou cle1 avec cle4
    cle3 avec cle2 ou cle3 avec cle4
    
    Donc on a decidé pour chaque combinaison de chiffrer le fichier avec les deux clé et on le rechiffre avec la 
    concatenation de deux mot de pass (le meme mecanisme de Q1) et on met le resultat (fichier chiffré) dans les deux clé usb.
    De cette facon pour déchiffrer on a besoin de presenter une combinaison avec les clé et les mot de pass et on poursuitles 
    mêmes étapes de la question1 pour dechiffrer.
    
    	
### Question 4 :
      
      ## fichier crypt.sh (pour chiffrer):
  
    	- On chiffre le fichier avec une clé keymaster générée et stockée sur le ramdisk et une fois on chiffre le fichier, on 
      supprime le fichier en clair qui est sur le disk.
    	- On genere une cle pour le premier responsable1 et on la met dans usb1.
    	- On genere une cle pour le deuxième responsable2 et on la met dans usb2.
    	- On genere une cle pour le representant du responsable 1 et on le met dans usb3.
    	- On genere une cle pourr le epresentant du responsable 2 et on la met dans usb4.
    	- On choisi les combinaisons et pout chaque combinaison on fait :
    	
    	  - On chiffre le fichier avec les deux cle d'une combinaison .
    		- On chiffre le fichier avec la concatenation de deux mot de pass.
    		- on copie le resultat (fichier chiffré) dans les deux clé usb.
    		- On supprime les fichiers dans ramdisk.
    	
      ## fichier decrypt.sh (pour déchiffre):
         pour dechiffrer on a besoin d'une combinaison (deux clé ,deux mot de pass)
         Donc dans notre script on a opté pour des questions de verrification pour s'assurer de la bonne combinaison puis une fois 
         on a reussi :
         
       - On demande au premier utilisateur de taper son mot de pass M1.
       - On demande au deuxieme utilisateur de de taper son mot de pass M2 .
       - On dechiffre le fichier avec la concatenation de deux mot de pass.
       - On redéchiffre le fichier avec le cle de resposable 2 .
       - On redéchiffre le fichier avec le cle de resposable 1 .
       - Enfin on décrypte le fichier chiffré avec la clé keymaster pour obtenir le fichier orginal.
       - On supprime les fichiers dans ramdisk
        

### Partie 3: Révocation de droit

### Question 5:

	
     
     	Pour retirer un représentant/reponsable on a besoin que les trois responsables soient présents avec leur mot de pass et 
       leurs clé
     	notre sulotion consiste à demander quelle représentant/reponsable on souhaite retiree et ensuite nous allons voir avec qui 
     	ce représentant/reponsable fait la combinaison pour supprimer tous les fichiers chiffés commun dans chaque cle usb,
     	de cette facon si le représentant/reponsable qui a été supprimé représente sa clé il peut pas déchiffrer le fichier
     	par ce qu'il a besoin d'un autre représentant/reponsable avec lui pour déchiffrer.
	
        
    	
### Question 6 :
       
       ## fichier crypt.sh (pour chiffre):
        
       c'est exactement le meme mecanisme qu'avant sauf qu'on a ajouté une facon pour memoriser les mot da pass chiffré
       comme ca lorsqu'on souhaite dechiffrer on peut veriffier si le représentant/reponsable est passé par ces étapes :
       on met chaque mot de pass dans un ficher dans le ramdisk et on le chiffre avec lui meme comme cle et on met le resultat 
       chiffré dans un dossier "motPass" de cette facon quand on veut déchiffrer on demande le mot de pass de chaque représentant/
       reponsable et on essaie de dechiffrer le fichier avec le mot de pass si ca marche donc c'est connu sinon un message d'erreur
       
       ## fichier revocation.sh :
       
       Dans ce fichier :
       - on demande quel représentant/reponsable.
       - on verifie les autres par leur mot de pass.
       - On supprime les fichiers.
      
        

        
        

        
      
        
        
