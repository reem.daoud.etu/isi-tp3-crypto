mkdir ramdisk
mkdir usb1
mkdir usb2
mkdir usb3
mkdir usb4
echo "technical password"
read teqPass
echo "Juridique password"
read jurPass
echo "représentant technique password"
read repteqPass
echo "représentant juridique password"
read repjurPass

declare -r motPass1=""$teqPass""""$jurPass""
declare -r motPass2=""$teqPass""""$repjurPass""
declare -r motPass3=""$repteqPass""""$jurPass""
declare -r motPass4=""$repteqPass""""$repjurPass""
openssl genrsa -out ./usb1/maCle.pem 1024
openssl genrsa -out ./usb2/maCle.pem 1024
openssl genrsa -out ./usb3/maCle.pem 1024
openssl genrsa -out ./usb4/maCle.pem 1024


openssl genrsa 2048 > ./ramdisk/keymaster.pem
openssl rsautl -encrypt -in ./disk/file.txt -inkey ./ramdisk/keymaster.pem -out ./disk/crypt_file.pem
rm ./disk/file.txt



openssl enc -aes-256-cbc -salt -in ./ramdisk/keymaster.pem -out ./ramdisk/keymasterCryptA -pass file:./usb1/maCle.pem
openssl enc -aes-256-cbc -salt -in ./ramdisk/keymasterCryptA -out ./ramdisk/keymasterCryptAB -pass file:./usb2/maCle.pem
openssl enc -aes-256-cbc -salt -in ./ramdisk/keymasterCryptAB -out ./ramdisk/keymasterCryptABpass.crypt -k $motPass1
cp ./ramdisk/keymasterCryptABpass.crypt ./usb1/
cp ./ramdisk/keymasterCryptABpass.crypt ./usb2/
rm ./ramdisk/keymasterCryptAB
rm ./ramdisk/keymasterCryptABpass.crypt
rm ./ramdisk/keymasterCryptA
openssl enc -aes-256-cbc -salt -in ./ramdisk/keymaster.pem -out ./ramdisk/keymasterCryptA -pass file:./usb1/maCle.pem
openssl enc -aes-256-cbc -salt -in ./ramdisk/keymasterCryptA -out ./ramdisk/keymasterCryptAD -pass file:./usb4/maCle.pem
openssl enc -aes-256-cbc -salt -in ./ramdisk/keymasterCryptAD -out ./ramdisk/keymasterCryptADpass.crypt -k $motPass2
cp ./ramdisk/keymasterCryptADpass.crypt ./usb1/
cp ./ramdisk/keymasterCryptADpass.crypt  ./usb4/
rm ./ramdisk/keymasterCryptAD
rm ./ramdisk/keymasterCryptA
rm ./ramdisk/keymasterCryptADpass.crypt


openssl enc -aes-256-cbc -salt -in ./ramdisk/keymaster.pem -out ./ramdisk/keymasterCryptB -pass file:./usb2/maCle.pem
openssl enc -aes-256-cbc -salt -in ./ramdisk/keymasterCryptB -out ./ramdisk/keymasterCryptBC -pass file:./usb3/maCle.pem
openssl enc -aes-256-cbc -salt -in ./ramdisk/keymasterCryptBC -out ./ramdisk/keymasterCryptBCpass.crypt -k $motPass3
cp ./ramdisk/keymasterCryptBCpass.crypt ./usb2/
cp ./ramdisk/keymasterCryptBCpass.crypt ./usb3/
rm ./ramdisk/keymasterCryptBC
rm ./ramdisk/keymasterCryptBCpass.crypt 
rm ./ramdisk/keymasterCryptB
openssl enc -aes-256-cbc -salt -in ./ramdisk/keymaster.pem -out ./ramdisk/keymasterCryptC -pass file:./usb3/maCle.pem
openssl enc -aes-256-cbc -salt -in ./ramdisk/keymasterCryptC -out ./ramdisk/keymasterCryptCD -pass file:./usb4/maCle.pem
openssl enc -aes-256-cbc -salt -in ./ramdisk/keymasterCryptCD -out ./ramdisk/keymasterCryptCDpass.crypt -k $motPass4
cp ./ramdisk/keymasterCryptCDpass.crypt ./usb3/
cp ./ramdisk/keymasterCryptCDpass.crypt ./usb4/
rm ./ramdisk/keymasterCryptCD
rm ./ramdisk/keymasterCryptCDpass.crypt
rm ./ramdisk/keymaster.pem
rm ./ramdisk/keymasterCryptC
