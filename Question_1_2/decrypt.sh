#!/bin/bash

echo "Technical password"
read teqPass
echo "Juridique password"
read legPass
declare -r motPass=""$teqPass""""$legPass""

openssl enc -in ./usb1/keymasterCryptABpass.crypt -out ./ramdisk/decryptMotdepass -d -aes256 -k $motPass
openssl enc -d -aes-256-cbc -in ./ramdisk/decryptMotdepass -out ./ramdisk/decryptB -pass file:./usb2/maCle.pem
openssl enc -d -aes-256-cbc -in ./ramdisk/decryptB -out ./ramdisk/decryptBA -pass file:./usb1/maCle.pem




openssl rsautl -decrypt -inkey ./ramdisk/decryptBA -in disk/crypt_file.pem -out ./disk/decrypt.txt>/dev/null
rm ./ramdisk/decryptMotdepass
rm ./ramdisk/decryptB
rm ./ramdisk/decryptBA 
rm ./disk/crypt_file.pem
echo "File decrypted successfully: \n" && cat disk/decrypt.txt
