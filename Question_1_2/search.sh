 #!/bin/bash
if [ $# -lt 1 ]; then    
         echo "A name must be provided"    
         exit 1
fi

grep $1":" disk/file.txt | while read -r line; do echo $line | cut -d ":" -f2; done

