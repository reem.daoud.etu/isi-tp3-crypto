#!/bin/bash
mkdir ramdisk
mkdir usb1
mkdir usb2
echo "Technical password"
read teqPass
echo "Juridique password"
read legPass

openssl genrsa 2048 > ./ramdisk/keymaster.pem

#On chiffre le fichier avec une keymaster puis on supprime le fichier en clair
openssl rsautl -encrypt -in ./disk/file.txt -inkey ./ramdisk/keymaster.pem -out ./disk/crypt_file.pem
rm ./disk/file.txt

#on genere des clés pour chaque responsable et on les place respectivement dans usb1 et usb2
openssl genrsa -out ./usb1/maCle.pem 1024
openssl genrsa -out ./usb2/maCle.pem 1024
declare -r motPass=""$teqPass""""$legPass""

#on crypte keymaster avec cle1 
openssl enc -aes-256-cbc -salt -in ./ramdisk/keymaster.pem -out ./ramdisk/keymasterCryptA -pass file:./usb1/maCle.pem
#on crypte kemaster deja chiffré avec cle1 avec cle2 
openssl enc -aes-256-cbc -salt -in ./ramdisk/keymasterCryptA -out ./ramdisk/keymasterCryptAB.crypt -pass file:./usb2/maCle.pem
#on chiffre le keymaster chifrée avec cle1 puis cle2 avec la concaténation des deux mots de passe 
openssl enc -aes-256-cbc -salt -in ./ramdisk/keymasterCryptAB.crypt -out ./ramdisk/keymasterCryptABpass.crypt -k $motPass

#suppression des fichiers
cp ./ramdisk/keymasterCryptABpass.crypt ./usb1/
cp ./ramdisk/keymasterCryptABpass.crypt ./usb2/
rm ./ramdisk/keymasterCryptA
rm ./ramdisk/keymasterCryptAB.crypt
rm ./ramdisk/keymasterCryptABpass.crypt
rm ./ramdisk/keymaster.pem 
echo "File encrypted successfully"
