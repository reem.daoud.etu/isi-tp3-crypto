mkdir ramdisk
mkdir usb1
mkdir usb2
mkdir usb3
mkdir usb4

#lecture des mdp pour chaque responsable 
echo "technical password"
read teqPass
echo "Juridique password"
read jurPass
echo "représentant technique password"
read repteqPass
echo "représentant juridique password"
read repjurPass
#sortie du resultat dans des nouveaux fichiers
echo $teqPass >> ./ramdisk/pass1.txt
echo $jurPass >> ./ramdisk/pass2.txt
echo $repteqPass >> ./ramdisk/pass3.txt
echo $repjurPass >> ./ramdisk/pass4.txt

#on chiffre chaque mot de passe en l'utilisant elle meme comme cle 
openssl enc -aes-256-cbc -salt -in ./ramdisk/pass1.txt -out ./motPass/pass1Cry.crypt -k $teqPass
openssl enc -aes-256-cbc -salt -in ./ramdisk/pass2.txt -out ./motPass/pass2Cry.crypt -k $jurPass
openssl enc -aes-256-cbc -salt -in ./ramdisk/pass3.txt -out ./motPass/pass3Cry.crypt -k $repteqPass
openssl enc -aes-256-cbc -salt -in ./ramdisk/pass4.txt -out ./motPass/pass4Cry.crypt -k $repjurPass

#concaténation de toutes les combinaisons possibles de mdp
declare -r motPass1=""$teqPass""""$jurPass""
declare -r motPass2=""$teqPass""""$repjurPass""
declare -r motPass3=""$repteqPass""""$jurPass""
declare -r motPass4=""$repteqPass""""$repjurPass""
#generation aleatoire de cle pour chaque responsable 
openssl genrsa -out ./usb1/maCle.pem 1024
openssl genrsa -out ./usb2/maCle.pem 1024
openssl genrsa -out ./usb3/maCle.pem 1024
openssl genrsa -out ./usb4/maCle.pem 1024

#on genere une keymaster avec laquelle on chiffre notre fichier sensible puis on le supprime et on garde la version cryptée du fichier
openssl genrsa 2048 > ./ramdisk/keymaster.pem
openssl rsautl -encrypt -in ./disk/file.txt -inkey ./ramdisk/keymaster.pem -out ./disk/crypt_file.pem
rm ./disk/file.txt


#On chiffre notre masterkey avec les deux clés puis le nouveau mdp concaténé 
openssl enc -aes-256-cbc -salt -in ./ramdisk/keymaster.pem -out ./ramdisk/keymasterCryptA -pass file:./usb1/maCle.pem
openssl enc -aes-256-cbc -salt -in ./ramdisk/keymasterCryptA -out ./ramdisk/keymasterCryptAB -pass file:./usb2/maCle.pem
openssl enc -aes-256-cbc -salt -in ./ramdisk/keymasterCryptAB -out ./ramdisk/keymasterCryptABpass.crypt -k $motPass1
cp ./ramdisk/keymasterCryptABpass.crypt ./usb1/
cp ./ramdisk/keymasterCryptABpass.crypt ./usb2/
rm ./ramdisk/keymasterCryptAB
rm ./ramdisk/keymasterCryptABpass.crypt
rm ./ramdisk/keymasterCryptA

openssl enc -aes-256-cbc -salt -in ./ramdisk/keymaster.pem -out ./ramdisk/keymasterCryptA -pass file:./usb1/maCle.pem
openssl enc -aes-256-cbc -salt -in ./ramdisk/keymasterCryptA -out ./ramdisk/keymasterCryptAD -pass file:./usb4/maCle.pem
openssl enc -aes-256-cbc -salt -in ./ramdisk/keymasterCryptAD -out ./ramdisk/keymasterCryptADpass.crypt -k $motPass2
cp ./ramdisk/keymasterCryptADpass.crypt ./usb1/
cp ./ramdisk/keymasterCryptADpass.crypt  ./usb4/
rm ./ramdisk/keymasterCryptAD
rm ./ramdisk/keymasterCryptADpass.crypt
rm ./ramdisk/keymasterCryptA

openssl enc -aes-256-cbc -salt -in ./ramdisk/keymaster.pem -out ./ramdisk/keymasterCryptB -pass file:./usb2/maCle.pem
openssl enc -aes-256-cbc -salt -in ./ramdisk/keymasterCryptB -out ./ramdisk/keymasterCryptBC -pass file:./usb3/maCle.pem
openssl enc -aes-256-cbc -salt -in ./ramdisk/keymasterCryptBC -out ./ramdisk/keymasterCryptBCpass.crypt -k $motPass3
cp ./ramdisk/keymasterCryptBCpass.crypt ./usb2/
cp ./ramdisk/keymasterCryptBCpass.crypt ./usb3/
rm ./ramdisk/keymasterCryptBC
rm ./ramdisk/keymasterCryptBCpass.crypt 
rm ./ramdisk/keymasterCryptB 

openssl enc -aes-256-cbc -salt -in ./ramdisk/keymaster.pem -out ./ramdisk/keymasterCryptC -pass file:./usb3/maCle.pem
openssl enc -aes-256-cbc -salt -in ./ramdisk/keymasterCryptC -out ./ramdisk/keymasterCryptCD -pass file:./usb4/maCle.pem
openssl enc -aes-256-cbc -salt -in ./ramdisk/keymasterCryptCD -out ./ramdisk/keymasterCryptCDpass.crypt -k $motPass4
cp ./ramdisk/keymasterCryptCDpass.crypt ./usb3/
cp ./ramdisk/keymasterCryptCDpass.crypt ./usb4/

rm ./ramdisk/keymasterCryptC 
rm ./ramdisk/keymasterCryptCD
rm ./ramdisk/keymasterCryptCDpass.crypt
rm ./ramdisk/pass1.txt
rm ./ramdisk/pass2.txt
rm ./ramdisk/pass3.txt
rm ./ramdisk/pass4.txt
rm ./ramdisk/keymaster.pem
